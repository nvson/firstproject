{
    'name': 'demo2',
    'description': 'Organize To-Do Tasks using Stages and Tags',
    'author': 'Daniel Reis',
    'depends': ['todo_app'],
    'depends': ['todo_app', 'mail'],
    'data': [
        'security/ir.model.access.csv',
        'views/todo_menu.xml',
        'views/todo_view.xml',
    ],
    'demo': [
            'data/todo.task.csv',
            'data/todo_task.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,
}
