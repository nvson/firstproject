{
    'name': 'demo',
    'version': '1.1',
    'summary': '',

    'description': """""",

    'depends': ['base'],

    'data': [
        'views/todo_menu.xml',
        'views/todo_view.xml',
        'views/res_partner_view.xml',

    ],
    'installable': True,
    'application': False,
    'auto_install': False,

}